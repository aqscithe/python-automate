import boto3
import click  #https://click.palletsprojects.com/en/7.x/ - pip install click
import botocore

# when designing a cli, think about the nouns and the verbs
# verbs will be your functions - list_instances, take_snapshot, list_volvumes
# nouns will be resources used - volumes, instances, snapshots, etc.

session = boto3.Session(profile_name='python')
ec2 = session.resource('ec2')

def get_updated_instance_state(instance):
    instances = ec2.instances.filter(Filters=[{'Name':'instance-id', 'Values':[instance.id]}])
    i = list(instances)[0]
    return i.state['Name']

def filter_instances(tag):
    if tag:
        filters = [
            {
                'Name':'tag:Project',
                'Values':[tag]
            }
        ]
        return ec2.instances.filter(Filters=filters)
    else:
        return ec2.instances.all()

@click.group()
def cli():
    "Shotty manages instances, volumes & snapshots"

@cli.group('volumes')
def volumes():
    "Commands for volumes"

@volumes.command('list')
@click.option('--project', default=None, help='list volumes of instances with tag Project:<name>')
def list_volumes(project):
    "List Volumes"

    instances = filter_instances(project)
    for i in instances:
        for v in i.volumes.all():
            attachments = { 'InstanceId': a['InstanceId'] for a in v.attachments or []}
            print(' | '.join((
                v.volume_id,
                attachments.get('InstanceId', '<not attached>'),
                v.state,
                v.encrypted and "Encrypted" or "Not Encrypted",
                str(v.size) + " GiB",
                ))
            )
    return

@cli.group('snapshots')
def snapshots():
    "Commands for snapshots"
@snapshots.command('list')
@click.option('--project', default=None, help='list most recent snapshot created from volumes of instances with tag Project:<name>')
@click.option('--all', 'list_all', default=False, is_flag=True, help='lists all snapshots, not just most recent')
def list_snapshots(project, list_all):
    "List Snapshots Created from Volumes of Tagged Instances"

    instances = filter_instances(project)
    for i in instances:
        for v in i.volumes.all():
            for s in v.snapshots.all():
                #boto3 returns the snapshots in reverse chronological order.
                #i.e. the most recent one first. so the following if statement will
                #return all in progress snapshots and the most recent completed
                #snapshot
                if s.state == 'completed' and not list_all:
                    print(' | '.join((
                        s.snapshot_id,
                        s.volume_id,
                        s.owner_id,
                        s.progress,
                        s.start_time.strftime('%c'),
                        s.state,
                        s.encrypted and "Encrypted" or "Not Encrypted"
                        ))
                    )
                    break
                else:
                    print(' | '.join((
                        s.snapshot_id,
                        s.volume_id,
                        s.owner_id,
                        s.progress,
                        s.start_time.strftime('%c'),
                        s.state,
                        s.encrypted and "Encrypted" or "Not Encrypted"
                        ))
                    )
    return

@snapshots.command('create')
@click.option('--project', default=None, help='create snapshots of volumes with tag Project:<name>')
def create_volume_snapshot(project):
    "Create Snapshots of Volumes"

    instances = filter_instances(project)
    for i in instances:
        try:
            i.stop()
            print("Stopping {0}...".format(i.id))
        except botocore.exceptions.ClientError as e:
            print("Could not stop {0}. An".format(i.id) + str(e))
            continue
        i.wait_until_stopped()
        for v in i.volumes.all():
            snapshots = list(v.snapshots.all())
            if snapshots and snapshots[0].state == 'pending':
                print("Latest snapshot of {0}, {1} still pending.".format(v.volume_id, snapshots[0].snapshot_id))
                print("Moving on to next volume.")
                continue
            v.create_snapshot(Description='Created by shotty')
            print("--Creating snapshot of {0}...".format(v.volume_id))
        i.start()
        print("--All snapshots in progress for {0}'s volumes. Restarting instance...".format(i.id))
    print("Snapshots creating for volumes of all of the specified instances")
    print("Run 'snapshots list' to see the progress")
    return

@cli.group('instances')
def instances():
    "Commands for instances"


@instances.command('list')
@click.option('--project', default=None, help='list instances with tag Project:<name>')
def list_instances(project):
    "List EC2 Instances"  #Python feature called a Doc String

    instances = filter_instances(project)
    for i in instances:
        tags = { t['Key']: t['Value'] for t in i.tags or []}
        print(' | '.join((
            i.instance_id,
            i.state['Name'],
            i.image_id,
            i.placement['AvailabilityZone'],
            i.public_dns_name,
            tags.get('Project', '<no project>')
            ))
        )


@instances.command('start')
@click.option('--project', default=None, help='start instances with tag Project:<name>')
def start_instances(project):
    "Start EC2 Instances"

    instances = filter_instances(project)
    for i in instances:
        try:
            response = i.start()
            print("Starting {0}... | HTTP Status Code: {1}".format(
                i.id,
                response['ResponseMetadata']['HTTPStatusCode'])
            )
        except botocore.exceptions.ClientError as e:
            print("Could not start {0}. An".format(i.id) + str(e))
            continue

@instances.command('stop')
@click.option('--project', default=None, help='stop instances with tag Project:<name>')
def stop_instances(project):
    "Stop EC2 Instances"

    instances = filter_instances(project)
    for i in instances:
        try:
            response = i.stop()
            print("Stopping {0}... | HTTP Status Code: {1}".format(
                i.id,
                response['ResponseMetadata']['HTTPStatusCode'])
            )
        except botocore.exceptions.ClientError as e:
            print("Could not stop {0}. An".format(i.id) + str(e))
            continue

if __name__ == '__main__':
    cli()
