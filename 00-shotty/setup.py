from setuptools import setup

setup(
    name='snapshotty',
    version='0.1',
    author='Leon Thompson',
    author_email='admin@allerium.net',
    description="Snapshotty is a tool to manage AWS snapshots.",
    license="GPLv3+",
    packages=['shotty'],
    url='https://bitbucket.org/aqscithe/python-automate',
    install_requires=[
        'click',
        'boto3'
    ],
    entry_points="""
        [console_scripts]
        shotty=shotty.shotty:cli
    """,
)
