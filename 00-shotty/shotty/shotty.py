import boto3
import click  #https://click.palletsprojects.com/en/7.x/ - pip install click
import botocore
import sys

# when designing a cli, think about the nouns and the verbs
# verbs will be your functions - list_instances, take_snapshot, list_volvumes
# nouns will be resources used - volumes, instances, snapshots, etc.

def get_user_profile(profile):
    try:
        session = boto3.Session(profile_name=profile)
        ec2 = session.resource('ec2')
    except botocore.exceptions.ProfileNotFound as e:
        sys.exit(str(e))
    return ec2

def is_project(tag, secondary):
    if not tag and not secondary:
        sys.exit("Project not set. Exiting...")
    return

def get_updated_instance_state(instance):
    instances = ec2.instances.filter(Filters=[{'Name':'instance-id', 'Values':[instance.id]}])
    i = list(instances)[0]
    return i.state['Name']

def filter_instances(tag, ec2):
    if tag:
        filters = [
            {
                'Name':'tag:Project',
                'Values':[tag]
            }
        ]
        if len(list(ec2.instances.filter(Filters=filters))):
            return ec2.instances.filter(Filters=filters)
        else:
            sys.exit("No instances with Project tag: {0}".format(tag))
    else:
        return ec2.instances.all()

@click.group()
def cli():
    "Shotty manages instances, volumes & snapshots"

@cli.group('volumes')
def volumes():
    "Commands for volumes"

@volumes.command('list')
@click.option('--project', default=None, help='list volumes of instances with tag Project:<name>')
@click.option('--profile', default=None, help='determines what AWS credentials to use')
def list_volumes(project, profile):
    "List Volumes"

    if profile:
        ec2 = get_user_profile(profile)
    else:
        ec2 = get_user_profile('python')
    instances = filter_instances(project, ec2)
    for i in instances:
        for v in i.volumes.all():
            attachments = { 'InstanceId': a['InstanceId'] for a in v.attachments or []}
            print(' | '.join((
                v.volume_id,
                attachments.get('InstanceId', '<not attached>'),
                v.state,
                v.encrypted and "Encrypted" or "Not Encrypted",
                str(v.size) + " GiB",
                ))
            )
    return

@cli.group('snapshots')
def snapshots():
    "Commands for snapshots"
@snapshots.command('list')
@click.option('--project', default=None, help='list most recent snapshot created from volumes of instances with tag Project:<name>')
@click.option('--all', 'list_all', default=False, is_flag=True, help='lists all snapshots, not just most recent')
@click.option('--profile', default=None, help='determines what AWS credentials to use')
def list_snapshots(project, list_all, profile):
    "List Snapshots Created from Volumes of Tagged Instances"

    if profile:
        ec2 = get_user_profile(profile)
    else:
        ec2 = get_user_profile('python')
    instances = filter_instances(project, ec2)
    for i in instances:
        for v in i.volumes.all():
            for s in v.snapshots.all():
                #boto3 returns the snapshots in reverse chronological order.
                #i.e. the most recent one first. so the following if statement will
                #return all in progress snapshots and the most recent completed
                #snapshot
                if s.state == 'completed' and not list_all:
                    print(' | '.join((
                        s.snapshot_id,
                        s.volume_id,
                        s.owner_id,
                        s.progress,
                        s.start_time.strftime('%c %Z'),
                        s.state,
                        s.encrypted and "Encrypted" or "Not Encrypted"
                        ))
                    )
                    break
                else:
                    print(' | '.join((
                        s.snapshot_id,
                        s.volume_id,
                        s.owner_id,
                        s.progress,
                        s.start_time.strftime('%c %Z'),
                        s.state,
                        s.encrypted and "Encrypted" or "Not Encrypted"
                        ))
                    )
    return

@snapshots.command('create')
@click.option('--project', default=None, help='create snapshots of volumes with tag Project:<name>')
@click.option('--force', is_flag=True,  help='runs command on ALL instances, regardless of project')
@click.option('--profile', default=None, help='determines what AWS credentials to use')
@click.option('--age', default=None, help='Will not initiate a snapshot on the volume if the last snapshot was created this many days ago or less')
def create_volume_snapshot(project, force, profile, age):
    "Create Snapshots of Volumes"

    if profile:
        ec2 = get_user_profile(profile)
    else:
        ec2 = get_user_profile('python')
    is_project(project, force)
    instances = filter_instances(project, ec2)
    print("")
    for i in instances:
        was_running = False
        if i.state['Name'] == 'running':
            was_running = True
            try:
                i.stop()
                print("Stopping {0}...".format(i.id))
            except botocore.exceptions.ClientError as e:
                print("Could not stop {0}. An".format(i.id) + str(e))
                continue
            i.wait_until_stopped()
        else:
            print("{0} already stopped. Proceeding with snapshot creation...".format(i.id))
        for v in i.volumes.all():
            snapshots = list(v.snapshots.all())
            if snapshots and snapshots[0].state == 'pending':
                print("Latest snapshot of {0}, {1} still pending.".format(v.volume_id, snapshots[0].snapshot_id))
                print("Moving on to next volume.")
                continue
            try:
                v.create_snapshot(Description='Created by shotty')
            except botocore.exceptions.ClientError as e:
                print(str(e))
            print("--Creating snapshot of {0}...".format(v.volume_id))
        if was_running:
            print("ALDKFA;DKFAFKLAD;FA;DKF")
            i.start()
            print("--All snapshots in progress for {0}'s volumes. Restarting instance...".format(i.id))
        else:
            print("--All snapshots in progress for {0}'s volumes.".format(i.id))
        print("")
    print("Snapshots creating for volumes of all of the specified instances")
    print("Run 'snapshots list' to see the progress")
    return

@cli.group('instances')
def instances():
    "Commands for instances"

@instances.command('list')
@click.option('--project', default=None, help='list instances with tag Project:<name>')
@click.option('--profile', default=None, help='determines what AWS credentials to use')
@click.option('--instance', default=None, help='specifies one instance')
def list_instances(project, profile, instance):
    "List EC2 Instances"  #Python feature called a Doc String

    if profile:
        ec2 = get_user_profile(profile)
    else:
        ec2 = get_user_profile('python')
    if not instance:
        instances = filter_instances(project, ec2)
    else:
        instances = [ec2.Instance(id=instance)]
    for i in instances:
        try:
            tags = { t['Key']: t['Value'] for t in i.tags or []}
            print(' | '.join((
                i.instance_id,
                i.state['Name'],
                i.image_id,
                i.placement['AvailabilityZone'],
                i.public_dns_name,
                tags.get('Project', '<no project>')
                ))
            )
        except botocore.exceptions.ClientError as e:
            print(str(e))


@instances.command('start')
@click.option('--project', default=None, help='start instances with tag Project:<name>')
@click.option('--force', is_flag=True,  help='runs command on ALL instances, regardless of project')
@click.option('--profile', default=None, help='determines what AWS credentials to use')
@click.option('--instance', default=None, help='specifies one instance')
def start_instances(project, force, profile, instance):
    "Start EC2 Instances"

    if profile:
        ec2 = get_user_profile(profile)
    else:
        ec2 = get_user_profile('python')
    is_project(project, force)
    if not instance:
        instances = filter_instances(project, ec2)
    else:
        instances = [ec2.Instance(id=instance)]
    for i in instances:
        try:
            response = i.start()
            print("Starting {0}... | HTTP Status Code: {1}".format(
                i.id,
                response['ResponseMetadata']['HTTPStatusCode'])
            )
        except botocore.exceptions.ClientError as e:
            print(str(e))
            continue

@instances.command('stop')
@click.option('--project', default=None, help='stop instances with tag Project:<name>')
@click.option('--force', is_flag=True,  help='runs command on ALL instances, regardless of project')
@click.option('--profile', default=None, help='determines what AWS credentials to use')
@click.option('--instance', default=None, help='specifies one instance')
def stop_instances(project, force, profile, instance):
    "Stop EC2 Instances"

    if profile:
        ec2 = get_user_profile(profile)
    else:
        ec2 = get_user_profile('python')
    is_project(project, force)
    if not instance:
        instances = filter_instances(project, ec2)
    else:
        instances = [ec2.Instance(id=instance)]
    for i in instances:
        try:
            response = i.stop()
            print("Stopping {0}... | HTTP Status Code: {1}".format(
                i.id,
                response['ResponseMetadata']['HTTPStatusCode'])
            )
        except botocore.exceptions.ClientError as e:
            print(str(e))
            continue

@instances.command('reboot')
@click.option('--project', default=None, help='reboot instances with tag Project:<name>')
@click.option('--force', is_flag=True,  help='runs command on ALL instances, regardless of project')
@click.option('--profile', default=None, help='determines what AWS credentials to use')
@click.option('--instance', default=None, help='specifies one instance')
def reboot_instances(project, force, profile, instance):
    "Reboot EC2 Instances"

    if profile:
        ec2 = get_user_profile(profile)
    else:
        ec2 = get_user_profile('python')
    is_project(project, force)
    if not instance:
        instances = filter_instances(project, ec2)
    else:
        instances = [ec2.Instance(id=instance)]
    for i in instances:
        try:
            response = i.reboot()
            print("Rebooting {0}... | HTTP Status Code: {1}".format(
                i.id,
                response['ResponseMetadata']['HTTPStatusCode'])
            )
        except botocore.exceptions.ClientError as e:
            print(str(e))
            continue

if __name__ == '__main__':
    cli()
