# Automating AWS w/ Python

### Configuring

The following scripts are configured using AWS cli.

`aws configure --profile python`

## 00-shotty

### About

Shotty is a command line tool that allows you to:
- list, start and stop instances.
- List Volumes
- list snapshots and create snapshots of volumes

If an instance is running or pending when the snapshot create command is ran, it
is appropriately stopped first before any snapshots begin.

### Running

```
pipenv run python shotty/shotty.py <command> <--project PROJECT>
pipenv run python shotty/shotty.py --help
```
*command* instances
*sub-commands* list, start, stop

*command* volumes
*sub-command* list

*command* snapshots
*sub-commands* list, create

*option* project

## 01-webotron
Webotron is a script that will sync a local directory to an S3 bucket.
It will optionally configure Route 53 and CloudFront as well.
